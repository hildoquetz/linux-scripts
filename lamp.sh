# Setup LAMP Server on Linux Ubuntu

	sudo apt-get update;
	sudo apt-get install apache2 -y;
	sudo apt-get install php -y;
	sudo apt-get install libapache2-mod-php -y;
	sudo apt-get install php-soap -y;
	sudo apt-get install mysql-server -y;
	sudo apt-get install php-mysql -y;
	sudo apt-get install phpmyadmin -y;

# setup

	#uncomment line 'extension=msql.so' on Dynamic Extensions
	sudo nano /etc/php/7.2/apache2/php.ini;

	#add line 'Include /etc/phpmyadmin/apache.conf'
	#add line 'ServerName myLocalHostName'
	sudo nano /etc/apache2/apache2.conf;

	#change priority of apache read files (index.php first)
	sudo nano /etc/apache2/mods-enabled/dir.conf

# restart apache

	sudo /etc/init.d/apache2 restart;

# show apache status configuration

	apachectl configtest;
