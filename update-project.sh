#!/bin/bash

#Basic script to backup database project and update on code repository.
#You need sudo privileges to execute this.
#By Hildo Quetz

	#Indicate name of project folder, database name and database user

	projectName="quetz.com.br";
	dbName="quetz_hml";
	dbUser="root";

	echo ;
	echo "--- YOU ARE NOW UPDATE '$projectName' PROJECT ---"
	echo ;
		
#Backup Database 

	echo "Now, type the password for '$dbUser' user to backup the database project";
	sudo mysqldump -u $dbUser -p $dbName > /var/www/html/$projectName/database/$dbName.sql;
	echo ;
	
#Commit the project

	cd /var/www/html/$projectName;
	sudo git add .;
	read -p "Say something about this update: " commit;
	sudo git commit -m "$commit";
	sudo git push -u origin master;
