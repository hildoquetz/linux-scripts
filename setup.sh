# This is a basic script to setup a Ubuntu Mate 64x environment
# By Hildo Quetz

#add new libs and get .deb

	sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys BBEBDCB318AD50EC6865090613B00F1FD2C19886 0DF731E45CE24F27EEEB1450EFDC8610341D9410;
	echo deb http://repository.spotify.com stable non-free | sudo tee /etc/apt/sources.list.d/spotify.list;
	wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -;
	echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list;
	wget http://kdl1.cache.wps.com/ksodl/download/linux/a21//wps-office_10.1.0.5707~a21_amd64.deb;
	wget https://downloads2.4kdownload.com/app/4kvideodownloader_4.3-1_amd64.deb;
	wget https://go.skype.com/skypeforlinux-64.deb;
	wget https://download.teamviewer.com/download/teamviewer_i386.deb;

#update and upgrade system

	sudo apt-get update;
	sudo apt-get upgrade -y;

#install programs

	sudo apt-get install htop -y;
	sudo apt-get install spotify-client -y;
	sudo apt-get install chromium-browser -y;
	sudo apt-get install caja-dropbox -y;
	sudo apt-get install filezilla -y;
	sudo apt-get install gimp -y;
	sudo apt-get install screenfetch -y;
	sudo apt-get install sublime-text -y;
	sudo apt-get install shutter -y;
	sudo apt-get install git -y;
	sudo apt-get install mysql-workbench -y;
	sudo apt-get install virtualbox -y;
	sudo apt-get install unrar -y;
	sudo apt-get install remmina -y;
	sudo apt-get install passwordsafe -y;
	sudo dpkg -i ./*;

#create some Web Apps

	echo "chromium-browser --app=https://soundcloud.com;" > soundcloud;
	echo "chromium-browser --app=https://web.whatsapp.com;" > whatsapp;
	echo "chromium-browser --app=https://itau.com.br;" > itau;
	echo "chromium-browser --app=https://evernote.com;" > evernote;
	echo "chromium-browser --app=https://app.asana.com;" > asana;
	echo "chromium-browser --app=https://twitter.com;" > twitter;
	echo "chromium-browser --app=https://pagseguro.com.br;" > pagseguro;
	echo "chromium-browser --app=https://pinterest.com;" > pinterest;
	echo "chromium-browser --app=https://duolingo.com;" > duolingo;
	echo "chromium-browser --app=http://www.br.de/static/radioplayer/player.html#/mediathek/audio/b5-aktuell-audio-livestream-100~radioplayer.json;" > b5;
	sudo chmod +x soundcloud whatsapp itau evernote asana twitter pagseguro pinterest duolingo b5;
	sudo mv soundcloud whatsapp itau evernote asana twitter pagseguro pinterest duolingo b5 /usr/local/bin;

#remove some programs

	sudo apt-get remove pluma -y;
	sudo apt-get remove thunderbird -y;
	sudo apt-get remove shotwell -y;
	sudo apt-get remove hexchat -y;
	sudo apt-get remove pidgin -y;

#custom update

	echo "sudo apt-get update; sudo apt-get upgrade -y; sudo apt-get autoremove -y; sudo apt-get autoclean -y;" > update;
	sudo chmod +x update;
	sudo mv update /usr/local/bin;
	update;

#show environment

	screenfetch;
