#Create client and project folder on Dropbox
	echo ;
	echo "--- CREATE CLIENT AND PROJECT FOLDER ---";
	echo ;
	echo "Give the name of Client and Project.";
	echo "Remember: the project name should be the same as your url.";
	echo ;
	read -p "Client Name: " clientName;
	read -p "Project Name: " projectName;
	pathToCreate="$clientName"/"$projectName";
	sudo mkdir -p $HOME/Dropbox/1-professional/1-QuetzAG/2-customers/$pathToCreate/bkp;
	sudo mkdir -p $HOME/Dropbox/1-professional/1-QuetzAG/2-customers/$pathToCreate/ftp;
	sudo mkdir -p $HOME/Dropbox/1-professional/1-QuetzAG/2-customers/$pathToCreate/img;
	cd /var/www/html;
	sudo wget https://wordpress.org/latest.zip;
	sudo unzip latest.zip;
	sudo mv wordpress $projectName;
	sudo mkdir $projectName/database;
	echo ;
	echo "The paths below has been created:";
	echo $HOME/Dropbox/1-professional/1-QuetzAG/2-customers/$pathToCreate;
	echo $HOME/Dropbox/1-professional/1-QuetzAG/2-customers/$pathToCreate/bkp;
	echo $HOME/Dropbox/1-professional/1-QuetzAG/2-customers/$pathToCreate/ftp;
	echo $HOME/Dropbox/1-professional/1-QuetzAG/2-customers/$pathToCreate/img;
	echo /var/www/html/$projectName;
	
	sudo reown;

#Create a new database 
	echo ;
	echo "--- CREATE A NEW DATABASE ---";
	echo ;
	createDB="create database qtz_$projectName";
	#echo "> Now, before you login on mysql, you can create the Database with 'CREATE DATABASE <database_name>;'.";
	#echo "> To see the databases, use 'SHOW DATABASES;' command.";
	#echo "> To drop a database, use 'DROP DATABASE <database_name>;' to do it.";
	#echo "> Use 'qtz_' was prefix of databases.";
	#echo ;
	mysql -u root -p -e "$createDB";

#Call the site

	firefox quetz.local/$projectName;